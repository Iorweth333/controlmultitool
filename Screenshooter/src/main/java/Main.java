import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        String dest = null;
        try {
            if (args.length != 1) {
                System.out.println("Please provide destination of the screenshot.");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                dest = br.readLine();
            }
            else dest = args[0];

            BufferedImage image = null;
            File screenshot = new File(dest);
            try {
                image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            } catch (HeadlessException | AWTException e) {
                // HeadlessException is thrown when code that is dependent on a keyboard,
                // display, or mouse is called in an environment that does not support a
                // keyboard, display, or mouse.
                // AWTException signals that an Abstract Window Toolkit exception has occurred.
                e.printStackTrace();
            }
            ImageIO.write(image, "png", screenshot);

        } catch (IOException | NullPointerException e) {
            System.out.println("Something failed...");
            e.printStackTrace();
        }
    }
}
