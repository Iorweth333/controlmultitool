import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.github.sarxos.webcam.Webcam;

public class CameraController {

    private Webcam webcam;

    public CameraController() {
        this.webcam = Webcam.getDefault();
        webcam.open();
    }

    public void setGoodResolution() { // will set best possible resolution unless it's bigger than 2592 x 1944
        Dimension[] nonStandardResolutions = new Dimension[] { new Dimension(2592, 1944) };

        try {
            webcam.close(); // Cannot change resolution when webcam is open
            webcam.setCustomViewSizes(nonStandardResolutions);
            webcam.setViewSize(nonStandardResolutions[0]);
            webcam.open();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error caught");
            System.out.println((e.getMessage()));
        }

    }

    public String takePicture() throws IOException {

        // get default webcam and open it

        // setGoodResolution();

        //check if webcam is open
        if (! webcam.isOpen()) webcam.open();
        // get image and close camera
        BufferedImage image = webcam.getImage();
        webcam.close();
        // save image to PNG file
        ImageIO.write(image, "PNG", new File("foto.png"));

        return "Image saved.";

    }

    public String takeAndSendPicture(Mail mail) throws Exception {
        //check if webcam is open
        if (! webcam.isOpen()) webcam.open();
        // get image and close camera
        BufferedImage image = webcam.getImage();
        webcam.close();

        // save image to PNG file
        File file = new File ("foto.png");
        ImageIO.write(image, "PNG", file);

        return mail.send ("Control Multitool", "This is who is in front of the computer now", file);

    }

}
