import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.io.File;

//https://www.tutorialspoint.com/java/java_sending_email.htm

public class Mail {

    private String to;
    private String from;
    private Properties properties;
    private Session session;

    public Mail(String to, String from, String password, String host) {
        this.to = to;
        this.from = from;
        // Get system properties
        this.properties = System.getProperties();
        // Setup mail server
        properties.setProperty("mail.smtp.host", host);
        properties.put("mail.smtp.ssl.trust", host);

        properties.setProperty("mail.user", from);
        properties.setProperty("mail.password", password);

        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.port", "587");

        // Get the default Session object.
        // @formatter:off
        session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(properties.getProperty("mail.user"),
                        properties.getProperty("mail.password"));
            }
        });
        // @formatter:on

    }

    public String send(String topic, String text) {

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(topic);

            // Now set the actual message
            message.setText(text);

            // Send message
            Transport.send(message);
            return "Message sent successfully.";
        } catch (MessagingException mex) {
            mex.printStackTrace();
            return "Exception caught.";
        }

    }

    public String send(String topic, String text, File file) {

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(topic);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText(text);

            // Create a multipart message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(file);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(file.getName());
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);
            return "Message sent successfully.";
        } catch (MessagingException mex) {
            mex.printStackTrace();
            return "Messaging Exception caught";
        }

    }

}
