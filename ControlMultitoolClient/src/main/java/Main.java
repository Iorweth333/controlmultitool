import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class Main {
    public static boolean ConnectionWorks = false;
    public static void main(String[] args) throws IOException {
        int portNumber = 4926;
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        String hostName = null;
        if (args.length != 1) {
            System.out.println("IP address (aka hostname) required. Please provide it. You can find it in an email from the server.");
            while (hostName == null || hostName.length() == 0)
                hostName = inputReader.readLine();    //in case that somebody just smashes ENTER
        } else hostName = args[0];

        Socket serverSocket = null;
        PrintWriter serverOut = null;
        BufferedReader serverIn = null;


        System.out.println("Hello! Trying to connect...");

        try {
            serverSocket = new Socket(hostName, portNumber);
            serverOut = new PrintWriter(serverSocket.getOutputStream(), true);
            serverIn = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
            ConnectionWorks = true;
            new Thread((new Receiver(serverIn))).start();

        } catch (Exception e) {
            System.out.println("Failed! Terminating.");
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Success! Connected.");

        System.out.println(serverIn.readLine());
        String input = inputReader.readLine();

        while (!input.equalsIgnoreCase("exit") && !input.equalsIgnoreCase("quit")) {
            try {
                serverOut.println(input);                                               //sending input to server
                input = inputReader.readLine();
                //todo: maybe some synchronization so that user didn't start typing command before previous is done?
            } catch (SocketException e) {
                System.out.println("Received Socket Exception. The program will terminate.");
                ConnectionWorks = false;
                break;
            }
        }

        serverSocket.close();
        System.out.println("Good bye.");

    }



}

