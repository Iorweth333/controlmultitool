import java.io.BufferedReader;
import java.io.IOException;

public class Receiver implements Runnable {
    private BufferedReader serverIn;

    public Receiver(BufferedReader serverIn) {
        this.serverIn = serverIn;
    }

    @Override
    public void run() {
        while (Main.ConnectionWorks)
            try {
                System.out.println(serverIn.readLine());
            } catch (IOException e) {
            if (! Main.ConnectionWorks){
                System.out.println("Error while receiving response from the server.");
                System.out.println("Error Message " + e.getMessage());
                Main.ConnectionWorks = false;
            }

            }
    }
}

